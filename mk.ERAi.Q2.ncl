; This is similar to mk.ERAi.Q1.ncl except it uses 
; potential temperature instead of dry static energy
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin

	flux_form = False


	var = "Q2"

	odir = "/Users/whannah/DYNAMO/ERAi/"
	ofile = odir+"ERAi."+var+".nc"
;====================================================================================================
;====================================================================================================
      opt  = True
      opt@lat1 = -15.
      opt@lat2 =  15.
      opt@lon1 =  50.
      opt@lon2 =  100.
      ;lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,70./)
      lev = (/70.,100.,125.,150.,175.,200.,250.,300.,400.,500.,600.,650.,700.,750.,800.,825.,850.,875.,900.,925.,950.,975./)
      lat = LoadERAilat(opt)
      lon = LoadERAilon(opt)	
        print("")
        print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:  "+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:  "+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+sprintf("%6.4g",max(lev))+"	:  "+sprintf("%6.4g",min(lev)))
        print("")
      latsz  = dimsizes(lat)
      lonsz  = dimsizes(lon)
      levsz  = dimsizes(lev)
      num_t    = (30+31+30+31+31)*4
;====================================================================================================
;====================================================================================================
        Ps    = LoadERAi("Ps"   ,opt)
        Q     = LoadERAi("Q"    ,opt)
        U     = LoadERAi("U"    ,opt)
        V     = LoadERAi("V"    ,opt)
        W     = LoadERAi("OMEGA",opt)
    	    	
        P  = conform(U,lev,1)*100.
        Po = 100000.
	
    
        S  = Lv*Q
        copy_VarCoords(U,S)
        delete(Q)
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;-----------------------------------------------------------   
        xvals = ispan(0,lonsz-1,1)
        Rvals = xvals(2:lonsz-1)
        Xvals = xvals(1:lonsz-2)
        Lvals = xvals(0:lonsz-3)
        yvals = ispan(0,latsz-1,1)
        Nvals = yvals(2:latsz-1)
        Cvals = yvals(1:latsz-2)
        Svals = yvals(0:latsz-3)
        
        dSdx  = new((/num_t,levsz,latsz-2,lonsz-2/),float)
        dSdy  = new((/num_t,levsz,latsz-2,lonsz-2/),float)

        deltax = conform(dSdx, tofloat( (lon(Rvals)+lon(Xvals))/2 - (lon(Lvals)+lon(Xvals))/2 ) ,3)
        deltay = conform(dSdx, tofloat( (lat(Nvals)+lat(Cvals))/2 - (lat(Svals)+lat(Cvals))/2 ) ,2)
        
        deltax = deltax*111000.*conform(dSdx,tofloat(cos(lat(Cvals)*3.14159/180.)),2)
        deltay = deltay*111000.
        
        if flux_form then
          SU = S
          SV = S
          SU = (/S*U/)
          SV = (/S*V/)
          dSdx  = ( (SU(:,:,Cvals,Rvals)+SU(:,:,Cvals,Xvals))/2  \
                   -(SU(:,:,Cvals,Lvals)+SU(:,:,Cvals,Xvals))/2   )/deltax
  
          dSdy  = ( (SV(:,:,Nvals,Xvals)+SV(:,:,Cvals,Xvals))/2  \
                   -(SV(:,:,Svals,Xvals)+SV(:,:,Cvals,Xvals))/2   )/deltay
            delete([/SU,SV/])
        else
          dSdx  = ( (S(:,:,Cvals,Rvals)+S(:,:,Cvals,Xvals))/2  \
                   -(S(:,:,Cvals,Lvals)+S(:,:,Cvals,Xvals))/2   )/deltax
  
          dSdy  = ( (S(:,:,Nvals,Xvals)+S(:,:,Cvals,Xvals))/2  \
                   -(S(:,:,Svals,Xvals)+S(:,:,Cvals,Xvals))/2   )/deltay
        end if
          
          delete([/deltax,deltay/])
      ;-----------------------------------------------------------
      ; truncate data at edges
      ;-----------------------------------------------------------
        tU  = U (:,:,Cvals,Xvals)
        tV  = V (:,:,Cvals,Xvals)
        tW  = W (:,:,Cvals,Xvals)
        tS  = S (:,:,Cvals,Xvals)
        tP  = P (:,:,Cvals,Xvals)
          delete([/U,V,W,S,P/])
        U  = tU
        V  = tV
        W  = tW
        S  = tS
        P  = tP
          delete([/tU,tV,tW,tS,tP/])
      ;-----------------------------------------------------------
      ; Total Tendency   
      ;-----------------------------------------------------------   
        dt = 12.*3600.
        dSdt = new((/num_t,levsz,latsz-2,lonsz-2/),float)
        ;dSdt(1:,:,:,:) = ( S(1:num_t-1,:,:,:) - S(0:num_t-2,:,:,:) ) / dt
        dSdt(1:num_t-2,:,:,:) = ( S(2:num_t-1,:,:,:) - S(0:num_t-3,:,:,:) ) / dt
      ;-----------------------------------------------------------
      ; Horizontal advection/convergence
      ;-----------------------------------------------------------
        if flux_form then
          delSV = dSdx + dSdy
          dSUdx = dSdx
          dSVdy = dSdy
        else
          VdelS = U*dSdx + V*dSdy
          UdSdx = U*dSdx
          VdSdy = V*dSdy
        end if
        delete([/dSdx,dSdy,U,V/])
      ;-----------------------------------------------------------
      ; Vertical advection/convergence
      ;-----------------------------------------------------------
        Tvals  = ispan(0,levsz-3,1)
        Zvals  = ispan(1,levsz-2,1) 
        Bvals  = ispan(2,levsz-1,1) 
        altdP  = ( P(:,Bvals,:,:) + P(:,Zvals,:,:) )/2. \
        	        -( P(:,Tvals,:,:) + P(:,Zvals,:,:) )/2.
        ;altdP  = P*( ( log(P(:,Bvals,:,:)) + log(P(:,Zvals,:,:)) )/2. \
        ;            -( log(P(:,Tvals,:,:)) + log(P(:,Zvals,:,:)) )/2.  )
        if flux_form then
          dSWdp  = new((/num_t,levsz,latsz-2,lonsz-2/),float)
          SW = S
          SW = (/ S*W /)
          dSWdp(:,Zvals,:,:)   	= ( ( SW(:,Bvals,:,:) + SW(:,Zvals,:,:) )/2. \
          		           -( SW(:,Tvals,:,:) + SW(:,Zvals,:,:) )/2. ) / altdP
        else
          dSdp   = new((/num_t,levsz,latsz-2,lonsz-2/),float)
          dSdp(:,Zvals,:,:) 	= ( ( S(:,Bvals,:,:) + S(:,Zvals,:,:) )/2. \
          		    	   -( S(:,Tvals,:,:) + S(:,Zvals,:,:) )/2. ) / altdP
          WdSdp  = W*dSdp
        end if
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        if flux_form then
          Q = dSdt + delSV + dSWdp
        else
          Q = -( dSdt + VdelS + WdSdp )
        end if
    
	Q!0 = "time"
	Q!1 = "lev"
        Q!2 = "lat"
        Q!3 = "lon"
        Q&lev = lev
        Q&lat = lat(Cvals)
        Q&lon = lon(Xvals)
        Q@long_name = "Apparent Heat Source"
        Q@units     = "J/kg/s"
        
        copy_VarCoords(Q,dSdt )
        dSdt@units = "J/kg/s"
        if flux_form then
          copy_VarCoords(dSdt,delSV)
          copy_VarCoords(dSdt,dSWdp)
          copy_VarCoords(dSdt,dSUdx)
          copy_VarCoords(dSdt,dSVdy)
        else
          copy_VarCoords(dSdt,VdelS)
          copy_VarCoords(dSdt,WdSdp)
          copy_VarCoords(dSdt,UdSdx)
          copy_VarCoords(dSdt,VdSdy)
        end if
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        if isfilepresent(ofile) then
          system("rm "+ofile) 
        end if
        outfile = addfile(ofile,"c")
        outfile->$var$ =  Q
        
      
      print(outfile)
      print("")
      print("  "+ofile)
      print("")
      
      delete([/W,S,P/])
      if flux_form then
        delete([/Q,dSdt,delSV,dSWdp/])
      else
        delete([/Q,dSdt,VdelS,WdSdp/])
      end if
;====================================================================================================
;====================================================================================================
end
