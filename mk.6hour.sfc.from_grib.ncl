load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
begin
  	idir = "/maloney-scratch/whannah/DYNAMO/ERAi/raw_grib/"
	odir = "/maloney-scratch/whannah/DYNAMO/ERAi/DYNAMO_data/"
	
	yrmn = (/201109,201110,201111,201112,201201/)
	days_per_month = (/30,31,30,31,31/)

	num_h = 4		
;=====================================================================================
; Switches & Constants
;=====================================================================================
  reorder_lon	= True

  ivar		= (/"TNSW","TNLW","SNSW","SNLW","LHF","SHF"/)
  ;ivar		= (/"SNLW"/)
  ovar 		= ivar
  
  hour 		= (/"6","12","18","24"/)

  num_v = dimsizes(ivar)
  num_m = dimsizes(yrmn)
;=====================================================================================
; Load data from Grib files
;=====================================================================================
do v = 0,num_v-1
	print("")
	print("	var 	"+ivar(v))
	print("")
  ofile = odir+"6hour.DYNAMO.ERAi."     +ovar(v)+""+".nc"

  do m = 0,num_m-1
  do hr = 0,3
    ;***********************************************
    ; Define times for output variable
    ;***********************************************
    if m.eq.0 then
      t1 = 0+hr
    else
      t1 = sum(days_per_month(:m-1))*num_h + hr
    end if
    t2 = sum(days_per_month(:m))*num_h + hr-num_h
    ;***********************************************
    ; Get variable names from grib file
    ;***********************************************
    ifile = idir+"DYNAMO.ERAi.1-deg."     +ivar(v)+"."+hour(hr)+"."+yrmn(m)+".grib"
    print("  converting "+ifile+"  >  "+ofile)
    grib_in	= addfile(ifile,"r")   

    names_in	= getfilevarnames(grib_in); extract all variable names 
    num_names 	= dimsizes(names_in)
 
    ; Find lat and lon coord variable names
    latchk = new(num_names,logical)
    lonchk = new(num_names,logical)
    do n = 0,num_names-1
    if isStrSubset(names_in(n),"lat") then
      latchk(n) = True
     end if
     if isStrSubset(names_in(n),"lon") then
       lonchk(n) = True
     end if
    end do
    lat_name = names_in(ind(latchk))
    lon_name = names_in(ind(lonchk))
    
   num_dims	= dimsizes(dimsizes(grib_in->$names_in(0)$))
     
	;----------------------------------------------------
	; Define Vout
	;----------------------------------------------------
	if (m.eq.0).and.(hr.eq.0) then
	 tsz 	   	 = sum(days_per_month)*num_h
   	 time    	 = fspan(0,tsz-1,tsz)*3.
   	 time!0  	 = "time"
   	 time@units 	 = "Hours since Oct. 1, 2011"    
	  ; Latitude
	  lat 		 = grib_in->$lat_name$(::-1)
	  lat!0	 = "lat"
	  lat@long_name = "Latitude"
	  lat@units	 = "degrees_north"
	  latsz 	 = dimsizes(lat)
	  ; Longitude
	  lon           = grib_in->$lon_name$
	  lon!0	 = "lon"
	  lon@long_name = "Longitude"
	  lon@units	 = "degrees_east"
	  lonsz 	 = dimsizes(lon) 
	  Vout = new((/tsz,latsz,lonsz/),float)
 	  Vout = 0.
	end if
	;----------------------------------------------------
	;----------------------------------------------------
    Vout(t1:t2:num_h,:,:) = grib_in->$names_in(0)$(:,::-1,:)
      delete([/names_in,latchk,lonchk/])
  end do   ; h = 0,3
  end do   ; m = 0,num_m-1
  ;================================================================
  ; fix units of accumulated fields
  ;================================================================
  tmp = Vout
  Vout(0::4,:,:) = ( tmp(0::4,:,:)  			)/(6.*3600.)
  Vout(1::4,:,:) = ( tmp(1::4,:,:) - tmp(0::4,:,:) 	)/(6.*3600.)
  Vout(2::4,:,:) = ( tmp(2::4,:,:) 			)/(6.*3600.)
  Vout(3::4,:,:) = ( tmp(3::4,:,:) - tmp(2::4,:,:) 	)/(6.*3600.)
  Vout@units = "W m**-2"
  delete(tmp)
  ;================================================================
  ;================================================================
    Vout!0 = "time"
    Vout!1 = "lat"
    Vout!2 = "lon"
    Vout&lat = lat
    Vout&lon = lon
  ;***********************************************
  ; Reorder the longitude if necessary.
  ;***********************************************
  if(reorder_lon)then
    print("    Reordering Longitude...")
    ; reorder so that lon -> 0:358.5 instead of -180:178.5
    ilon = lon
    delete(lon)
    lon = ilon + 180
    j = ispan(0,latsz-1,1)
      if num_dims.eq.4 then
        tmp = Vout(:,:,j,:)
        Vout(:,:,j,       :lonsz/2-1) = tmp(:,:,j,lonsz/2:         )
        Vout(:,:,j,lonsz/2:         ) = tmp(:,:,j,       :lonsz/2-1)
      else
        tmp = Vout;(:,j,:)
        Vout(:,j,       :lonsz/2-1) = tmp(:,j,lonsz/2:         )
        Vout(:,j,lonsz/2:         ) = tmp(:,j,       :lonsz/2-1)
      end if
      delete(tmp)
   lon!0 = "lon"
   lon&lon = lon
   Vout&lon = lon
  end if
  ;-----------------------------------------------
  ; Write to file
  ;-----------------------------------------------
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")
  outfile->$ovar(v)$ = Vout
  outfile->lat = lat
  outfile->lon = lon
  print("")
    delete([/Vout,lat,lon/])
   
end do  ; v = 0,num_v-1
;=====================================================================================
;=====================================================================================
end
