load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ERAi.ncl"
begin

	var = "LvQ"
	odir  = "/maloney-scratch/whannah/DYNAMO/ERAi/"
	ofile = odir+"ERAi.budget."+var+".nc"
;====================================================================================================
;====================================================================================================
      opt  = True
      opt@lat1 = -15.
      opt@lat2 =  15.
      opt@lon1 =   0.
      opt@lon2 = 360.
      ;lev = (/975.,950.,925.,900.,875.,850.,825.,800.,750.,700.,650.,600.,500.,400.,300.,250.,200.,175.,150.,125.,100.,70./)
      lev = (/70.,100.,125.,150.,175.,200.,250.,300.,400.,500.,600.,650.,700.,750.,800.,825.,850.,875.,900.,925.,950.,975./)
      lat = LoadERAilat(opt)
      lon = LoadERAilon(opt)	
        print("")
        print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:  "+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:  "+sprintf("%6.4g",max(lon)))
        print("    lev = 	"+sprintf("%6.4g",max(lev))+"	:  "+sprintf("%6.4g",min(lev)))
        print("")
      num_lat  = dimsizes(lat)
      num_lon  = dimsizes(lon)
      num_lev  = dimsizes(lev)
      num_t    = (30+31+30+31)*4

      wcprc = systemfunc("date")
      wctot = systemfunc("date")
;====================================================================================================
;====================================================================================================
	Ps    = LoadERAi("Ps"   ,opt)
	Q     = LoadERAi("Q"    ,opt)
	U     = LoadERAi("U"    ,opt)
        V     = LoadERAi("V"    ,opt)
        W     = LoadERAi("OMEGA",opt)
        LHFLX = LoadERAi("LHF"  ,opt)
	PRECT = LoadERAiPrecip(lat,lon)*Lv*1000./(24.*3600.*1000.)	; convert mm/day => m/s => W/m^2 (use Lv and density)
	
	LHFLX = (/-LHFLX/)	; Fluxes are positive downward by default, so
				; we need to flip the sign for the residual
	
	wallClockElapseTime(wcprc, "Loading vars", 0)
	wcprc = systemfunc("date")
	
        P  = conform(Q,lev,1)*100.
        dP = new(dimsizes(Q),float)
        Ps3D = conform(P,Ps,(/0,2,3/))
        dP(:,1:num_lev-2,:,:) = ( P(:,0:num_lev-3,:,:)+P(:,1:num_lev-2,:,:) )/2. - \
                                ( P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:) )/2.
        dP(:,0,:,:)  = where( Ps.gt.P(:,0,:,:) , Ps - (P(:,0,:,:)+P(:,1,:,:))/2. , dP@_FillValue)
        dP(:,1:num_lev-2,:,:) = where( (Ps3D(:,1:num_lev-2,:,:).gt.P(:,1:num_lev-2,:,:)).and.	\
                                       (Ps3D(:,1:num_lev-2,:,:).lt.P(:,0:num_lev-3,:,:)), 	\
                                        Ps3D(:,1:num_lev-2,:,:)-(P(:,1:num_lev-2,:,:)+P(:,2:num_lev-1,:,:))/2., \
                                        dP  (:,1:num_lev-2,:,:) )

        S  =  Q *Lv
        	copy_VarCoords(U,S)
        	
        delete([/Q,Ps3D/])
      wallClockElapseTime(wcprc, "data prep", 0)
      wcprc = systemfunc("date")
      ;-----------------------------------------------------------   
      ; Calculate horizontal gradients
      ;-----------------------------------------------------------   
        xvals = ispan(0,num_lon-1,1)
	Rvals = (xvals+num_lon+1)%num_lon
	Lvals = (xvals+num_lon-1)%num_lon
	yvals = ispan(0,num_lat-1,1)
	Nvals = yvals(2:num_lat-1)
	Cvals = yvals(1:num_lat-2)
	Svals = yvals(0:num_lat-3)
        S_dx  = new((/num_t,num_lev,num_lat-2,num_lon/),float)
        S_dy  = new((/num_t,num_lev,num_lat-2,num_lon/),float)
        SU_dx  = new((/num_t,num_lev,num_lat-2,num_lon/),float)
        SV_dy  = new((/num_t,num_lev,num_lat-2,num_lon/),float)

        dx = tofloat( (lon(2)-lon(0))*111000.*conform(S_dx,cos(lat(Cvals)*3.14159/180.),2) )
        dy = tofloat( (lat(2)-lat(0))*111000. )
        
	S_dx  = (  S(:,:,Cvals,Rvals)- S(:,:,Cvals,Lvals) )/dx
        S_dy  = (  S(:,:,Nvals,:)    - S(:,:,Svals,:)     )/dy
        SU_dx  = (  S(:,:,Cvals,Rvals)*U(:,:,Cvals,Rvals)- S(:,:,Cvals,Lvals)*U(:,:,Cvals,Lvals) )/dx
        SV_dy  = (  S(:,:,Nvals,:)*V(:,:,Nvals,:)    - S(:,:,Svals,:)*V(:,:,Svals,:)     )/dy
          delete([/dx,dy/])
        wallClockElapseTime(wcprc, "Horz derivative", 0)
        wcprc = systemfunc("date")
      ;-----------------------------------------------------------
      ; truncate data at edges
      ;-----------------------------------------------------------
        tdP = dP(:,:,Cvals,:)
        tU  = U (:,:,Cvals,:)
        tV  = V (:,:,Cvals,:)
        tW  = W (:,:,Cvals,:)
        tS  = S (:,:,Cvals,:)
        tP  = P (:,:,Cvals,:)
        tLH = LHFLX (:,Cvals,:)
        tPR = PRECT (:,Cvals,:)
        
          delete([/dP,U,V,W,S,P,LHFLX,PRECT/])
        dP = tdP
        U  = tU
        V  = tV
        W  = tW
        S  = tS
        P  = tP
        LHFLX = tLH
        PRECT = tPR
          delete([/tdP,tU,tV,tW,tS,tP,tLH,tPR/])
        wallClockElapseTime(wcprc, "data truncation", 0)
        wcprc = systemfunc("date")
      ;-----------------------------------------------------------   
      ; Total tendency
      ;-----------------------------------------------------------   
        STEND = new(dimsizes(S),float)
        STEND(1:,:,:,:) = ( S(1:,:,:,:) - S(0:num_t-2,:,:,:) ) / ( 6.*60.*60. )
        LvQDT  = dim_sum_n(STEND*dP/g,1)
        LvQvi  = dim_sum_n(S    *dP/g,1)
          delete([/STEND/])    
        wallClockElapseTime(wcprc, "total tendency", 0)
        wcprc = systemfunc("date")  
      ;-----------------------------------------------------------
      ; Horizontal advective tendencies
      ;-----------------------------------------------------------
        VdelQ = dim_sum_n( (U*S_dx+V*S_dy)*dP/g,1)
        delVQ = dim_sum_n( ( SU_dx+ SV_dy)*dP/g,1)
        udQdx = dim_sum_n( (U*S_dx)       *dP/g,1)
        vdQdy = dim_sum_n( (V*S_dy)       *dP/g,1)
          delete([/U,V,S_dx,S_dy,SU_dx,SV_dy/])
        wallClockElapseTime(wcprc, "horz adv tendency", 0)
        wcprc = systemfunc("date")
      ;-----------------------------------------------------------
      ; Vertical advective tendency
      ;-----------------------------------------------------------
        altdP =  P(:,0:num_lev-3,:,:) - P(:,2:num_lev-1,:,:)
        dQdp  = (S(:,0:num_lev-3,:,:) - S(:,2:num_lev-1,:,:) ) / altdP
        WdQdp = dim_sum_n( W(:,1:num_lev-2,:,:)*(  dQdp)*dP(:,0:num_lev-3,:,:)/g,1)
          delete([/P,W,S,altdP,dQdp/])
        wallClockElapseTime(wcprc, "vert adv tendency", 0)
        wcprc = systemfunc("date")
      ;-----------------------------------------------------------
      ; Column mass
      ;-----------------------------------------------------------
        COLDP = dim_sum_n(dP/g,1)
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        LvQDT!0 = "time"
        LvQDT!1 = "lat"
        LvQDT!2 = "lon"
        LvQDT&lat = lat(Cvals)
        LvQDT&lon = lon
        copy_VarCoords(LvQDT,LvQvi)
        	copy_VarCoords(LvQDT,VdelQ)
        	copy_VarCoords(LvQDT,delVQ)
        	copy_VarCoords(LvQDT,udQdx)
        	copy_VarCoords(LvQDT,vdQdy)
        	copy_VarCoords(LvQDT,WdQdp)
        	copy_VarCoords(LvQDT,LHFLX)
        	copy_VarCoords(LvQDT,PRECT)
        	copy_VarCoords(LvQDT,COLDP)
        
        unit_str = "W m^-2"
        LvQvi@long_name = "Column LvQ"
        COLDP@long_name = "Column Mass"
        LvQDT@long_name = "Column LvQ Tendency"
        VdelQ@long_name = "adv. LvQ tend. (VdelQ)"
        delVQ@long_name = "adv. LvQ tend. (delVQ)"
        udQdx@long_name = "adv. LvQ tend. (udQdx)"
        vdQdy@long_name = "adv. LvQ tend. (vdQdy)"
        WdQdp@long_name = "adv. LvQ tend. (WdQdp)"
        LHFLX@long_name = "Sfc LH Flux"
        PRECT@long_name = "Precipitation"
        LvQvi@units	= "J m^-2"
        COLDP@units	= "kg m^-2"
        LvQDT@units	= unit_str
        VdelQ@units	= unit_str
        delVQ@units	= unit_str
        udQdx@units	= unit_str
        vdQdy@units	= unit_str
        WdQdp@units	= unit_str
        LHFLX@units	= unit_str
        PRECT@units	= unit_str
        wallClockElapseTime(wcprc, "var names", 0)
        wcprc = systemfunc("date")
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
        print("	writing file >	"+ofile)
        if isfilepresent(ofile) then
          system("rm "+ofile) 
        end if
        outfile = addfile(ofile,"c")
        outfile->LvQvi = LvQvi
        outfile->COLDP = COLDP
        outfile->LvQDT = LvQDT
        outfile->VdelQ = VdelQ
        outfile->delVQ = delVQ
        outfile->udQdx = udQdx
        outfile->vdQdy = vdQdy
        outfile->WdQdp = WdQdp
        outfile->LHFLX = LHFLX
        outfile->PRECT = PRECT
      ;-----------------------------------------------------------
      ;-----------------------------------------------------------
      wallClockElapseTime(wcprc, "output file written", 0)
      wallClockElapseTime(wctot, "Total budget calculation", 0)
        delete([/dP,LvQvi,COLDP,LvQDT,VdelQ,delVQ,udQdx,vdQdy,WdQdp,LHFLX,PRECT/])
    ;***********************************************************************************
    ;***********************************************************************************
;====================================================================================================
;====================================================================================================
end
