#!/usr/bin/env python
#===========================================================================================
# This script retrieves ECMWF Interim ReAnalysis (ERAi) data. Files are put in the 
# ./raw_grib/ folder. Each year of data will consist of 4 files, one for each time of day
# available. The ncl script mk.yearly_files.from_grib.ncl will combine data into daily
# means and write out netCDF files.
#
#    Mar, 2013	Walter Hannah 		Colorado State University
#===========================================================================================
from ecmwfapi import ECMWFDataServer
import sys
import numpy as np
#===========================================================================================
# Setup ECMWF connection
#===========================================================================================
server = ECMWFDataServer()
#===========================================================================================
# Initialize variables
#===========================================================================================
#---------------------------------------------
# start and end years
#---------------------------------------------
yrmn    = [201201]
num_y   = len(yrmn)
end_day = [31]
#---------------------------------------------
# time (UTC) & Forecast hour (i.e. step)
#---------------------------------------------
times = ["00","00","12","12"]
steps = ["06","12","06","12"]
#---------------------------------------------
# Specify which variabels to fetch
#---------------------------------------------
var = ["TNSW","TNLW","SNSW","SNLW","LHF","SHF"]
#var = ["PRECT"]
#===========================================================================================	
# Loop through each variable
#===========================================================================================
for v in xrange(len(var)):
	
	if var[v] == "SHF"   : par = ["146.128"]	# Surface Sensible Heat FLux
	if var[v] == "LHF"   : par = ["147.128"]	# Surface Latent Heat Flux
	if var[v] == "SNSW"  : par = ["176.128"]	# Surface Net Shortwave
	if var[v] == "SNLW"  : par = ["177.128"]	# Surface Net Longwave
	if var[v] == "TNSW"  : par = ["178.128"]	# Top Net Shortwave
	if var[v] == "TNLW"  : par = ["179.128"]	# Top Net Longwave
	if var[v] == "PRECT" : par = ["228.128"]	# Total Precip
	
	print "" 
	print "  ----------------------------------"
	print "   ----------------------------------"
	print "    var    => ",var[v]
	print "    par    => ",par[0]
	print "  ----------------------------------"
	print "  ----------------------------------"
	
	for y in xrange(0,num_y):
		for t in xrange(len(times)):
			date   = str(yrmn[y])+"01/to/"+str(yrmn[y])+str(end_day[y])
			time   = times[t]
			step   = steps[t]
			fxtm   = "%02d" % ( int(times[t])+int(steps[t]) )
			domain = "20/-180/-20/179"
			target = "/Users/whannah/DYNAMO/ERAi/raw_grib/DYNAMO.ERAi.1-deg."+var[v]+"."+time+"."+str(yrmn[y])+".grib";
			#-------------------------------------------------
			#-------------------------------------------------
			print ""
			print "     time   => ",time
			print "     date   => ",date
			print "     domain => ",domain
			print "     target => ",target
			print ""
			#-------------------------------------------------
			#-------------------------------------------------
			server.retrieve({
			  'grid'      : "1/1",
			  'dataset'   : "interim",
			  'stream'    : "oper",
			  'step'      :  step,
			  'type'      : "fc",
			  'param'     : par[0],
			  'levtype'   : "sfc", 
			  'area'      : domain,
			  'time'      : time,
			  'date'      : date,
			  'target'    : target, 
			  })
#===========================================================================================
#===========================================================================================
