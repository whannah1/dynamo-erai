#!/usr/bin/env python
#===========================================================================================
# This script retrieves ECMWF Interim ReAnalysis (ERAi) data. Files are put in the 
# ./raw_grib/ folder. Each year of data will consist of 4 files, one for each time of day
# available. The ncl script mk.yearly_files.from_grib.ncl will combine data into daily
# means and write out netCDF files.
#
#    Mar, 2013	Walter Hannah 		Colorado State University
#===========================================================================================
from ecmwfapi import ECMWFDataServer
import sys
import numpy as np
#===========================================================================================
# Setup ECMWF connection
#===========================================================================================
server = ECMWFDataServer()
#===========================================================================================
# Initialize variables
#===========================================================================================
#---------------------------------------------
# time period
#---------------------------------------------
yrmn    = [201110,201111,201112,201201]
end_day = [31,30,31,31]
#yrmn    = [201110]
#end_day = [31]
num_y   = len(yrmn)
#---------------------------------------------
# time of day (Zulu)
#---------------------------------------------
times = ["00","06","12","18"]
#times = ["00"]
#---------------------------------------------
# Specify which variabels to fetch
#---------------------------------------------
#var = ["U","V","OMEGA","T","Q","GEO","Ps"]
var = ["V"]
#===========================================================================================	
# Loop through each variable
#===========================================================================================
for v in xrange(len(var)):
	
	if var[v] == "GEO"   : par = ["129.128","pl"]	# Geopotential
	if var[v] == "T"     : par = ["130.128","pl"]	# Temperature
	if var[v] == "U"     : par = ["131.128","pl"]	# U-Wind
	if var[v] == "V"     : par = ["132.128","pl"]	# V-Wind
	if var[v] == "Q"     : par = ["133.128","pl"]	# Specific Humidity
	if var[v] == "OMEGA" : par = ["135.128","pl"]	# Vertical Pressure Velocity
	if var[v] == "DIV"   : par = ["154.128","pl"]	# Divergence
	if var[v] == "Ps"    : par = ["134.128","sfc"]	# Surface Pressure
	if var[v] == "CWV"   : par = ["137.128","sfc"]	# Column Water Vapor
	if var[v] == "SST"   : par = [ "34.128","sfc"]	# Sea-Surface Temperature
	
	print "" 
	print "  ----------------------------------"
	print "   ----------------------------------"
	print "    var    => ",var[v]
	print "    par    => ",par[0]
	print "  ----------------------------------"
	print "  ----------------------------------"
	
	for y in xrange(0,num_y):
		for t in xrange(len(times)):
			date   = str(yrmn[y])+"01/to/"+str(yrmn[y])+str(end_day[y])
			time   = times[t]
			#domain = "20/-180/-20/178.5"
			domain = "40/-180/-40/178.5"
			target = "/Users/whannah/DYNAMO/ERAi/raw_grib/DYNAMO.ERAi.1-deg."+var[v]+"."+time+"."+str(yrmn[y])+".grib";
			lev = "70/100/125/150/175/200/250/300/400/500/600/650/700/750/800/825/850/875/900/925/950/975"	
			#-------------------------------------------------
			#-------------------------------------------------
			print ""
			print "     time   => ",time
			print "     date   => ",date
			print "     domain => ",domain
			print "     target => ",target
			print ""
			#-------------------------------------------------
			#-------------------------------------------------
			server.retrieve({
			  'grid'      : "1/1",
			  'dataset'   : "interim",
			  'stream'    : "oper",
			  'step'      :  0,
			  'param'     : par[0],
			  'levtype'   : par[1], 
			  'levelist'  : lev,
			  'area'      : domain,
			  'time'      : time,
			  'date'      : date,
			  'target'    : target, 
			  })
#===========================================================================================
#===========================================================================================
